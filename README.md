# Coder React Sample Application

Coder Enterprise Demo Environment

[![Open in Coder Enterprise](https://cdn.coder.com/embed-button.svg)](https://demo.cdr.dev/environments/git?org=5e274cb6-8ad3877561fcf4c2c4a95f3e&image=5ebeec5a-f3a509b57c20dfd57ea2e4c7&tag=latest&service=gitlab&repo=git@gitlab.com:cdecelles/coder-react.git)

Coder Enterprise CJD Demo Environment

[![Open in Coder Enterprise](https://cdn.coder.com/embed-button.svg)](https://cjd.demo.coder.com/environments/git?org=5f70d455-6555965f30e7d3686aa3a784&image=5fa445ec-19773892bf035ad9a8a08240&tag=latest&service=github&repo=git@gitlab.com:cdecelles/coder-react.git)

